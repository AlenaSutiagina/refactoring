import java.util.Enumeration;
import java.util.Vector;

public class Customer {
	private final String name;
	private final Vector<Rental> rentals = new Vector<>();

	public Customer(String name) {
		this.name = name;
	}

	public void addRental(Rental rental) {
		rentals.addElement(rental);
	}

	public String getName() {
		return name;
	}

	public String getRentalRecordForCustomer() {
		double totalAmount = 0;
		int frequentRenterPoints = 0;

		Enumeration<Rental> rentalEnumeration = rentals.elements();

		StringBuilder result = new StringBuilder("Rental Record for " + this.getName() + "\n");
		result.append("\t" + "Title" + "\t" + "\t" + "Days" + "\t" + "Amount" + "\n");

		while (rentalEnumeration.hasMoreElements()) {
			Rental nextRental = rentalEnumeration.nextElement();

			double thisAmount = nextRental.amountFor();

			frequentRenterPoints++;

			if ((nextRental.getMovie().getPriceCode() == Movie.NEW_RELEASE) && nextRental.getDaysRented() > 1)
				frequentRenterPoints++;

			result.append("\t").append(nextRental.getMovie().getTitle()).append("\t").append("\t").append(nextRental.getDaysRented()).append("\t").append(thisAmount).append("\n");
			totalAmount += thisAmount;
		}

		result.append("Amount owed is ").append(totalAmount).append("\n");
		result.append("You earned ").append(frequentRenterPoints).append(" frequent renter points");
		return result.toString();
	}

}