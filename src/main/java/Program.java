public class Program {

	public static void main(String[] args) {

		String result;
		System.out.println("Welcome to the Movie Store");

		Movie firstMovie = new Movie("Movie1", 1);
		Movie secondMovie = new Movie("Movie2", 2);

		Rental firstRental = new Rental(firstMovie, 10);
		Rental secondRental = new Rental(secondMovie, 5);

		Customer customer = new Customer("Joe");
		customer.addRental(firstRental);
		customer.addRental(secondRental);

		System.out.println("Let's get the Statement");

		result = customer.getRentalRecordForCustomer();

		System.out.println(result);
	}
}